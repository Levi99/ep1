#ifndef CATEGORIA_HPP
#define CATEGORIA_HPP

#include<string>

using namespace std;

class Categoria {

	private:
		string nome_categoria;
		string tipo_produto;
	public:
		Categoria();
		Categoria(string nome_categoria, string tipo_produto);
		~Categoria();

		string get_nome_categoria();
		void set_nome_categoria(string nome_categoria);
		string get_tipo_produto();
		void set_tipo_produto(string tipo_produto);


};


#endif
