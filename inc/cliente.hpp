#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include<string>
#include"pessoa.hpp"

using namespace std;

class Cliente : public Pessoa {


	public:
	Cliente();
	Cliente(string nome, long int cpf, string telefone, string email, string endereco, int cep);
	Cliente(string nome);
	~Cliente();


};

#endif
