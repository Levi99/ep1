#ifndef FUNCIONARIO_HPP
#define FUNCIONARIO_HPP

#include<string>
#include"pessoa.hpp"

using namespace std;

class Funcionario : public Pessoa {

	private:
		int f_identificacao; //identificação numeral do funcionário

	public:
		Funcionario();
		Funcionario(string nome, long int cpf, string telefone, string email, string endereco, int cep, int f_identificacao);
		~Funcionario();

		int get_f_identificacao();
		void set_f_identificacao(int f_identificacao);

		void ponto_entsai(); //função que me da quanto tempo o funcionário trabalhou


};


#endif
