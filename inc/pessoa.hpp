#ifndef PESSOA_HPP
#define PESSOA_HPP

#include<string>

using namespace std;

class Pessoa {


	private:
		string nome;
		long int cpf;
		string telefone;
		string email;
		string endereco;
		int cep;

	public:
		Pessoa();
		Pessoa(string nome, long int cpf, string telefone, string email, string endereco, int cep);
		Pessoa(string nome);
		~Pessoa();

		string get_nome();
		void set_nome(string nome);
		long int get_cpf();
		void set_cpf(long int cpf);
		string get_telefone();
		void set_telefone(string telefone);
		string get_email();
		void set_email(string email);
		string get_endereco();
		void set_endereco(string endereco);
		int get_cep();
		void set_cep(int cep);




};


#endif
