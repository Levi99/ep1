#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include<string>
#include"categoria.hpp"

using namespace std;

class Produto : public Categoria {

	private:
		int p_identificacao; // Identificação numeral do produto
		string nome_produto;
		int quantidade;
	public:
		Produto();
		Produto(string nome_categoria, string tipo_produto, int p_identificacao, string nome_produto, int quantidade);
		~Produto();

		int get_p_identificacao();
		void set_p_identificacao(int p_identificacao);
		string get_nome_produto();
		void set_nome_produto(string nome_produto);
		int get_quantidade();
		void set_quantidade(int quantidade);

};

#endif
