#include"categoria.hpp"
#include<iostream>
#include<string>

using namespace std;

Categoria::Categoria(){
	set_nome_categoria("");
	set_tipo_produto("");
}

Categoria::Categoria(string nome_categoria, string tipo_produto){
	set_nome_categoria(nome_categoria);
	set_tipo_produto(tipo_produto);
}

Categoria::~Categoria(){
	cout << "Categoria destruida" << endl;
}

string Categoria::get_nome_categoria(){
	return nome_categoria;
}

void Categoria::set_nome_categoria(string nome_categoria){
	this->nome_categoria = nome_categoria;
}

string Categoria::get_tipo_produto(){
	return tipo_produto;
}

void Categoria::set_tipo_produto(string tipo_produto){
	this->tipo_produto = tipo_produto;
}
