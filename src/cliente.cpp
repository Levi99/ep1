#include"pessoa.hpp"
#include"cliente.hpp"
#include<string>
#include<iostream>

using namespace std;

Cliente::Cliente(){
	set_nome("");
	set_cpf(0);
	set_telefone("");
	set_email("");
	set_endereco("");
	set_cep(0);
}

Cliente::Cliente(string nome, long int cpf, string telefone, string email, string endereco, int cep){
	set_nome(nome);
        set_cpf(cpf);
        set_telefone(telefone);
        set_email(email);
        set_endereco(endereco);
        set_cep(cep);
}

Cliente::Cliente(string nome){
	set_nome(nome);
        set_cpf(0);
        set_telefone("");
        set_email("");
        set_endereco("");
        set_cep(0);
}


