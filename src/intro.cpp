#include"intro.hpp"
#include"pessoa.hpp"
#include"funcionario.hpp"
#include"cliente.hpp"
#include<string>
#include<iostream>
#include<fstream>

using namespace std;

template <typename Num>
Num Numero(){
	while(true){
		Num entrada;
	        cin.clear();
		cin >> entrada;
		if(cin.fail()){
			cin.clear();
			cout << "Entrada inválida. Insira Novamente: ";
			cin.ignore(1, '\n');
		}
		else{
			return entrada;
			break;
		}
	}
}

string getString(){

	while(true){
	cin.clear();
	string entrada;
	try{
		getline(cin, entrada);
	}
	catch(const ios_base::failure& e){
		cin.clear();
		cin.ignore(32767, '\n');
		cout << "Entrada Inválida - " << e.what() << " - Insira Novamente: ";
	}
	return entrada;
}
}

void Intro::set_menu(){

	char prime;
	while(true){
		system("clear");
		cout << "                        " << " _________________________________ " << endl;
                cout << "                        " << "|______Bem - Vindo! Meu Caro______|" << endl;
                cout << "                        " << "|_________________________________|" << endl;
                cout << "                        " << "|_________________________________|" << endl;
                cout << "                        " << "|_________Projeto EP1_____________|" << endl;
                cout << "                        " << "|________Padaria Salvação_________|" << endl;
                cout << "                        " << "|_________________________________|" << endl;
                cout << "                        " << "|_________________________________|" << endl;
                cout << "                        " << "|_______Prime C para continuar____|" << endl;
                cout << "                        " << "|_________________________________|" << endl;

                prime = Numero<char>();
                if(prime == 'c'){
                        system("clear");
                        break;
                }
                else{
                        cout << "                        " << "Entrada Inválida - Insira Novamente: ";
                }
        }
	while(true){

		cout << "                        " << " _________________________________ " <<  endl;
		cout << "                        " << "|______Aluno: Levi Queiroz________|" << endl;
		cout << "                        " << "|_________________________________|" << endl;
		cout << "                        " << "|__________OO_FGA_2019.2__________|" << endl;
		cout << "                        " << "|_________Projeto EP1_____________|" << endl;
		cout << "                        " << "|_________________________________|" << endl;
		cout << "                        " << "|_____Matrícula: 170108341________|" << endl;
		cout << "                        " << "|_______Data: 02/09/2019__________|" << endl;
		cout << "                        " << "|_______Prime C para continuar____|" << endl;
		cout << "                        " << "|_________________________________|" << endl;

		prime = Numero<char>();
		if(prime == 'c'){
			system("clear");
			break;
		}
		else{
			cout << "                        " << "Entrada Inválida - Insira Novamente: ";
		}
	}
}

void Intro::login(){
	char prime;
	string login, senha;
	while(true){
		cout << "                        " << " _________________________________ " << endl;
                cout << "                        " << "|           Bem - Vindo           |" << endl;
                cout << "                        " << "|               ao                |" << endl;
                cout << "                        " << "|       Sistema Operacional       |" << endl;
                cout << "                        " << "|               da                |" << endl;
                cout << "                        " << "|        Padaria Salvação         |" << endl;
                cout << "                        " << "|                                 |" << endl;
                cout << "                        " << "|  Selecione:                     |" << endl;
                cout << "                        " << "|  a. Cliente      b. Funcionário |" << endl;
                cout << "                        " << "|_________________________________|" << endl;

                prime = Numero<char>();
                if(prime == 'a'){
                        system("clear");
                        cout << "                        " << " _________________________________ " << endl;
                	cout << "                        " << "|           Bem - Vindo           |" << endl;
                	cout << "                        " << "|             Cliente             |" << endl;
                	cout << "                        " << "|                                 |" << endl;
			cout << "                        " << "|   Login:  "<< login <<"                      |" << endl;
                	cout << "                        " << "|                                 |" << endl;
                	cout << "                        " << "|   Senha:  "<< senha  <<"                      |" << endl;
                	cout << "                        " << "|            prime l              |" << endl;
                	cout << "                        " << "|   Se não é cadastado prime c    |" << endl;
                	cout << "                        " << "|_________________________________|" << endl;
			login = getString();
			senha = getString();
			char prime1 = Numero<char>();
			if(prime1 == 'l'){
				cout << "Obrigado pelo teste" << endl;
				break;
			}
			else if(prime1 == 'c'){
				cout << "Em Manutenção" << endl;
				break;
			}
			else{
                        cout << "                        " << "Entrada Inválida - Insira Novamente: ";
                }

                }
		else if(prime == 'b'){
			system("clear");
			cout << "Em manutenção" << endl;
			break;

		}
                else{
                        cout << "                        " << "Entrada Inválida - Insira Novamente: ";
                }
		break;
        }
			}
