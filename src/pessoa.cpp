#include"pessoa.hpp"
#include<iostream>
#include<string>


using namespace std;

Pessoa::Pessoa(){

	nome = "";
	cpf = 0;
	telefone = "";
	email = "";
	endereco = "";
	cep = 0;

}

Pessoa::Pessoa(string nome, long int cpf, string telefone, string email, string endereco, int cep){

	set_nome("");
	set_cpf(0);
	set_telefone("");
	set_email("");
	set_endereco("");
	set_cep(0);
}

Pessoa::Pessoa(string nome){

	set_nome("");
        set_cpf(cpf);
        set_telefone(telefone);
        set_email(email);
        set_endereco(endereco);
        set_cep(cep);

}

Pessoa::~Pessoa(){
	cout << "Pessoa Consumada" << endl;
}

string Pessoa::get_nome(){
	return nome;
}

void Pessoa::set_nome(string nome){
	this->nome = nome;
}

long int Pessoa::get_cpf(){
	return cpf;
}

void Pessoa::set_cpf(long int cpf){
	this->cpf = cpf;
}

string Pessoa::get_telefone(){
        return telefone;
}

void Pessoa::set_telefone(string telefone){
        this->telefone = telefone;
}

string Pessoa::get_email(){
        return email;
}

void Pessoa::set_email(string email){
        this->email = email;
}

string Pessoa::get_endereco(){
        return endereco;
}

void Pessoa::set_endereco(string endereco){
        this->endereco = endereco;
}

int Pessoa::get_cep(){
        return cep;
}

void Pessoa::set_cep(int cep){
        this->cep = cep;
}

