#include"categoria.hpp"
#include"produto.hpp"
#include<string>
#include<iostream>

using namespace std;

Produto::Produto(){
		
		set_nome_categoria("");
                set_tipo_produto("");
		set_p_identificacao(0);
		set_nome_produto("");
		set_quantidade(0);

}

Produto::Produto(string nome_categoria, string tipo_produto, int p_identificacao, string nome_produto, int quantidade){
		set_nome_categoria("");
                set_tipo_produto("");
                set_p_identificacao(0);
                set_nome_produto("");
                set_quantidade(0);
}

Produto::~Produto(){


	cout << "Produto esgotado" << endl;

}

int Produto::get_p_identificacao(){
	return p_identificacao;
}

void Produto::set_p_identificacao(int p_identificacao){
	this->p_identificacao = p_identificacao;
}

string Produto::get_nome_produto(){
	return nome_produto;
}

void Produto::set_nome_produto(string nome_produto){
	this->nome_produto = nome_produto;
}

int Produto::get_quantidade(){
	return quantidade;
}

void Produto::set_quantidade(int quantidade){
	this->quantidade = quantidade;
}
